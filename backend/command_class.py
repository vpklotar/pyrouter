import sys

class Command:
    command_string = ""
    command_description = ""
    command_function = None

class CommandGroup:
    command_string = ""
    command_description = ""
    command_function = None
    commands = {}

    def register_command(self, command):
        self.commands.update({command.command_string: command})

    def unregister_command(self, command):
        self.commands.update(command.command_string, command)

    def get_commands(self):
        return self.commands