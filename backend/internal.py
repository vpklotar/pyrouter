import sys
import os

BREADCRUM = ""

# Hash map to store built-in function name and reference as key and value
built_in_cmds = {}
current_command_group = {}

def execute(cmd_tokens):
    # Fork a child shell process
    # If the current process is a child process, its `pid` is set to `0`
    # else the current process is a parent process and the value of `pid`
    # is the process id of its child process.
    pid = os.fork()

    if pid == 0:
    # Child process
        # Replace the child shell process with the program called with exec
        os.execvp(cmd_tokens[0], cmd_tokens)
    elif pid > 0:
    # Parent process
        while True:
            # Wait response status from its child process (identified with pid)
            wpid, status = os.waitpid(pid, 0)

            # Finish waiting if its child process exits normally
            # or is terminated by a signal
            if os.WIFEXITED(status) or os.WIFSIGNALED(status):
                break

def set_breadcrum(brd):
    global BREADCRUM
    BREADCRUM = brd

def get_breadcrum():
    return BREADCRUM

def register_command(command):
    built_in_cmds.update({command.command_string: command})

def unregister_command(command):
    #TODO: Create function
    print()

def set_current_command_set(group):
    global current_command_group
    current_command_group = group

def get_current_command_set():
    global current_command_group
    return current_command_group

#def show_help():
#   global built_in_cmds
#
#   for command in built_in_cmds:
#       print(command.command_string + "\t\t\t" + command.command_description)

def get_interfaces():
    interfaces = []

    f = os.popen('ip addr | grep "[0-9]:\ " | grep -E "[A-Za-z0-9]+:" | cut -d: -f2')
    result = [s.strip() for s in f.read().splitlines()]
    return result