import sys

def print_list(cmd_list):
	maximum = 0
	pre_def = 35
	for command in cmd_list:
		if len(command) > maximum:
			maximum = len(command)

	for command in cmd_list:
		sys.stdout.write(command)
		for x in range(0, pre_def - len(command)):
			sys.stdout.write(" ")
		print(cmd_list[command])