from backend.command_class import *
from commands.show_interface import *
import sys
import os

class show_interfaces(Command):
	def __init__(self):
		self.command_string = "interfaces"
		self.command_description = "Display interfaces attatch to device and status"
		self.command_function = self.interfaces

	def interfaces(self, args):
		show_interface("show interfaces")