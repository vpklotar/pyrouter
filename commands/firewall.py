import sys
from backend import *
from backend.internal import *
from commands.show_hostname import get_hostname
from commands.firewall_show_subcommands import *
from commands.firewall_subcommands import *

class Firewall_cmd(CommandGroup):
	def func(self, args):
		set_current_command_set(self.get_commands())
		set_breadcrum(get_hostname() + ">firewall")

	def __init__(self):
		self.command_string = "firewall"
		self.command_description = "Enter firewall configuration mode"
		self.command_function = self.func
		self.commands = {}
		self.register_subcommands()

	def register_subcommands(self):
		fw_sh_cmd = Firewall_show_cmd()
		self.register_command(fw_sh_cmd)

		zone = Zone_cmd()
		self.register_command(zone)

class Firewall_show_cmd(CommandGroup):
	def __init__(self):
		self.command_string = "show"
		self.command_function = self.func
		self.command_description = "Used to show firewall information"
		self.commands = {}
		self.register_subcommands()

	def func(self, args):
		print("Use any of the subcommands to display information")

	def register_subcommands(self):
		sh_zone = show_zone()
		self.register_command(sh_zone)