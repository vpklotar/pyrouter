import sys
from backend import *

class Exit(Command):
	def __init__(self):
		self.command_string = "exit"
		self.command_function = exit
		self.command_string = "Exit this command shell"
		self.command_desc = "Command used to exit this command shell"

	def exit(args):
		sys.exit()