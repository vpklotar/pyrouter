import sys
from backend.command_class import *
from backend.internal import *
from commands.interface_subcommands import *
from show_hostname import get_hostname


def enter(args):
    set_breadcrum(get_hostname() + ">interface>" + args[1])

class Interface_Command(CommandGroup):
    def get_name(self):
        return self.command_string

    def enter(self, args):
        set_breadcrum(get_hostname() + ">interface>" + self.get_name())
        set_current_command_set(self.get_commands())
        self.register_subcommands()

    def register_subcommands(self):
        int_status = interface_status()
        int_status.interface = self.get_name()
        register_command(int_status)

        up_cmd = up(self.get_name())
        register_command(up_cmd)

        down_cmd = down(self.get_name())
        register_command(down_cmd)

        address_cmd = address(self.get_name())
        register_command(address_cmd)

    def __init__(self):
        self.command_function = self.enter
        self.commands = {}


class Interface_CommandGroup(CommandGroup):
    def register_subcommands(self):
        interfaces = get_interfaces()
        for port in interfaces:
            interface = Interface_Command()
            interface.command_string = port
            interface.command_description = "Enter configuration mode for " + port
            self.register_command(interface)

    def show_syntax(self, args):
        print("Syntax: interface [name]")
        print("Valid interfaces: ")
        for interface in get_interfaces():
            print(interface)

    def __init__(self):
        self.command_string = "interface"
        self.command_description = "Sub-commands allows entering and configuring interfaces"
        self.command_function = self.show_syntax
        self.commands = {}
        self.register_subcommands()