import os
import sys
from backend.command_class import *

class Clear(Command):
	def func(self, args):
		os.system("clear")

	def __init__(self):
		self.command_string = "clear"
		self.command_description = "Clear the shell"
		self.command_function = self.func