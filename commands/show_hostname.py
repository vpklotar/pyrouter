#!/usr/bin/env python

import sys
import os
from backend import *

class show_hostname(Command):
	def __init__(self):
		self.command_string = "hostname"
		self.command_description = "Show's the hostname of the current PyRouter"
		self.command_function = self.func

	def func(self, args):
		print os.uname()[1]

def get_hostname():
	return os.uname()[1]