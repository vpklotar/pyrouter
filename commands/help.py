import sys
from backend.command_class import *
from backend.internal import *
from backend import *

class Help(Command):
	def __init__(self):
		self.command_string = "help"
		self.command_description = "Used to display the help information"
		self.command_function = self.show_help

	def show_help(self, args):
		global built_in_cmds
		#for cmd in built_in_cmds:
		#	print(cmd + "\t\t\t" + built_in_cmds[cmd].command_description)
		cmds = {}
		for cmd in built_in_cmds:
			cmds[cmd] = built_in_cmds[cmd].command_description
		print_list(cmds)