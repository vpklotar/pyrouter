import sys
from backend.internal import *
from commands.show_subcommands import *
from commands.firewall_show_subcommands import *
from commands.show_hostname import *

class Show_CommandGroup(CommandGroup):
    def register_subcommands(self):
        sh_ints = show_interfaces()
        self.register_command(sh_ints)

        fw_zone = show_zone()
        self.register_command(fw_zone)

        show_host = show_hostname()
        self.register_command(show_host)

    def show_syntax(self, args):
        print("Possible show commands: ")
        for cmd in self.get_commands():
            print(cmd)

    def __init__(self):
        self.commands = {}
        self.command_string = "show"
        self.command_description = "Show different system data"
        self.command_function = self.show_syntax
        self.register_subcommands()