from backend.command_class import *
from commands.show_interface import *
import sys
import os

class up(Command):
	def __init__(self, interface):
		self.command_string = "up"
		self.command_function = self.up
		self.interface = interface

	def up(self, args):
		if get_interface_info(self.interface)["state"] == "UP":
			return
		os.popen('ip link set ' + self.interface + ' up')

class down(Command):
	def __init__(self, interface):
		self.interface = interface
		self.command_string = "down"
		self.command_function = self.down

	def down(self, args):
		if get_interface_info(self.interface)["state"] == "DOWN":
			return
		os.popen('ip link set ' + self.interface + ' down')