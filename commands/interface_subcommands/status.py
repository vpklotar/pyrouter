from backend.command_class import *
from commands.show_interface import *
import sys

class interface_status(Command):
	interface = ""

	def func(self, args):
		print_header()
		print_interface_info_brief(self.interface)

	def __init__(self):
		self.command_string = "status"
		self.command_function = self.func
		self.command_description = "Show brief status about the current interface"