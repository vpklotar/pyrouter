from backend.command_class import *
from commands.show_interface import *
import sys
import os

class address(CommandGroup):
    interface = ""
    def func(self, args):
        print("Syntax: address [ [add|remove] [ip-address](/CIDR) | dhcp ]")

    def __init__(self, interface):
        self.command_function = self.func
        self.command_string = "address"
        self.command_desciption = "Manage addresses on this interface"
        self.interface = interface
        self.register_subcommands()

    def register_subcommands(self):
        add_cmd = add(self.interface)
        self.register_command(add_cmd)

        remove_cmd = remove(self.interface)
        self.register_command(remove_cmd)

        dhcp_cmd = dhcp(self.interface)
        self.register_command(dhcp_cmd)


class add(Command):
    interface = ""
    def func(self, args):
        os.system("ip addr add " + args[2] + " dev " + self.interface)

    def __init__(self, interface):
        self.command_string = "add"
        self.command_desciption = "Add an ip-address to an interface"
        self.command_function = self.func
        self.interface = interface

class remove(Command):
    interface = ""
    def func(self, args):
        os.system("ip addr del " + args[2] + " dev " + self.interface)

    def __init__(self, interface):
        self.command_string = "remove"
        self.command_desciption = "Remove an ip-address from an interface"
        self.command_function = self.func
        self.interface = interface

class dhcp(Command):
    interface = ""
    def func(self, args):
        info = get_interface_info(self.interface)
        if info['cidr'] != '/0':
            remove(self.interface).command_function(info['ip'] + info['cidr'])
        os.system("dhclient " + self.interface)
    
    def __init__(self, interface):
        self.interface = interface
        self.command_string = "dhcp"
        self.command_desciption = "Removes current address and applies DHCP-leasing"
        self.command_function = self.func