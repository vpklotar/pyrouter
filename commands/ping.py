import sys
from backend.internal import *
from backend import *
from subprocess import Popen

class ping_command(Command):
    def show_syntax(self, args):
        if len(args) < 2:
            print("Syntax: ping [address]")
        else:
            f = os.popen('ping -c 2 ' + args[1])
            #f = Popen(['ping', '-c 2', 'google.se'])
            print(f.read().strip())

    def __init__(self):
        self.commands = {}
        self.command_string = "ping"
        self.command_description = "Ping an IP-address or a DNS-name"
        self.command_function = self.show_syntax