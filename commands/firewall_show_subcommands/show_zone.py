import sys
import os
from backend import *

class show_zone(CommandGroup):
    def func(self, args):
        print(args)
        if len(args) == 3:
            f = os.popen('iptables -nvL ' + args[2].upper())
            print(f.read().strip())
        else:
            f = os.popen('iptables -nvL')
            print(f.read().strip())

    def RegisterSubcommands(self):
        f = os.popen('iptables -nvL 2>&1 | grep Chain | cut -d" " -f 2')
        read = f.read().strip()
        for line in read.splitlines():
            z = zone(line)
            self.register_command(z)


    def __init__(self):
        self.command_string = "zone"
        self.command_description = "Show zone information, rules and such"
        self.command_function = self.func
        self.RegisterSubcommands()


class zone(Command):
    zone = ""

    def __init__(self, zone):
        self.command_string = zone
        self.command_description = "Show zone rules for zone " + zone
        self.command_function = self.func
        self.zone = zone

    def func(self, args):
        f = os.popen('iptables -nvL ' + self.zone)
        print(f.read().strip())