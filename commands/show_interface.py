#!/usr/bin/env python

import sys
from backend import *

subnets = {
    "255.255.255.254": "/31",
    "255.255.255.252": "/30",
    "255.255.255.248": "/29",
    "255.255.255.240": "/28",
    "255.255.255.224": "/27",
    "255.255.255.192": "/26",
    "255.255.255.128": "/25",
    "255.255.255.0": "/24",
    "255.255.254.0": "/23",
    "255.255.252.0": "/22",
    "255.255.248.0": "/21",
    "255.255.240.0": "/20",
    "255.255.224.0": "/19",
    "255.255.192.0": "/18",
    "255.255.128.0": "/17",
    "255.255.0.0": "/16",
    "255.254.0.0": "/15",
    "255.252.0.0": "/14",
    "255.248.0.0": "/13",
    "255.240.0.0": "/12",
    "255.224.0.0": "/11",
    "255.192.0.0": "/10",
    "255.128.0.0": "/9",
    "255.0.0.0": "/8",
    "254.0.0.0": "/7",
    "252.0.0.0": "/6",
    "248.0.0.0": "/5",
    "240.0.0.0": "/4",
    "224.0.0.0": "/3",
    "192.0.0.0": "/2",
    "128.0.0.0": "/1",
    "0.0.0.0": "/0"
}

def show_interface(args):
    #info = get_interface_info("ens18")
    #print(info["ip"] + " " + info["netmask"])
    print_header()
    interfaces = get_interfaces()
    for interface in interfaces:
        print_interface_info_brief(interface)

def print_header():
    print("INTERFACE     IP-ADDRESS     NETMASK/CIDR     STATUS")

def print_interface_info_brief(interface):
    info = get_interface_info(interface)
    CIDR = ""
    if info["netmask"] != "":
        CIDR = subnets[info["netmask"]]

    print(info["name"] + "     " + info["ip"] + "     " + info["netmask"] + CIDR + "     " + info["state"])

def get_interface_info(interface):
    info = {"name": interface}

    f = os.popen('ip addr show dev ' + interface + ' | grep "inet " | cut -d" " -f6 | cut -d"/" -f1')
    info["ip"] = f.read().strip()
    if info['ip'] == "":
        info['ip'] = "0.0.0.0"

    f = os.popen('ifconfig ' + interface + ' | grep "Mask" | cut -d: -f4')
    info["netmask"] = f.read().strip()

    if info['netmask'] != "":
        info['cidr'] = subnets[info['netmask']]
    else:
        info['cidr'] = '/0'
        info['netmask'] = "0.0.0.0"

    f = os.popen('ifconfig ' + interface + ' | grep "Bcast" | cut -d: -f3 | cut -d" " -f1')
    info["broadcast"] = f.read().strip()

    f = os.popen('ip addr sh dev ' + interface + ' | grep -Eo " (UP|DOWN)"')
    info["state"] = f.read().strip()

    return info


def get_interfaces():
    interfaces = []

    f = os.popen('ip addr | grep "[0-9]:\ " | grep -E "[A-Za-z0-9]+:" | cut -d: -f2')
    result = [s.strip() for s in f.read().splitlines()]
    return result