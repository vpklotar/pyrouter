import sys
from backend import *
from getch import getch, pause

class Zone_cmd(CommandGroup):
    def __init__(self):
        self.command_string = "zone"
        self.command_description = "create, modify and remove zones"
        self.command_function = self.func
        self.commands = {}
        self.register_subcommands()

    def func(self, args):
        print("Syntax: zone [create|rename|delete] [zone]")

    def register_subcommands(self):
        z_add = Zone_add_cmd()
        self.register_command(z_add)

        z_remove = Zone_remove_cmd()
        self.register_command(z_remove)

class Zone_add_cmd(Command):
    def __init__(self):
        self.command_string = "create"
        self.command_description = "Used to create a zone"
        self.command_function = self.func

    def func(self, args):
        f = os.popen('iptables -nvL ' + args[2].upper() + ' 2>&1 | grep "No chain"')
        result = f.read().strip()
        
        if bool(result):
            # The chain doesn't exist already, create it
            c = os.popen('iptables -N ' + args[2].upper())
        else:
            for x in args:
                print(x)
            print("That zone already exist")

class Zone_remove_cmd(Command):
    def __init__(self):
        self.command_string = "delete"
        self.command_description = "Used to create a zone"
        self.command_function = self.func

    def func(self, args):
        f = os.popen('iptables -nvL ' + args[2].upper() + ' 2>&1 | grep "No chain"')
        result = f.read().strip()
        
        if not bool(result):
            print("Are you sure you want to remove the zone and all it's rules? (Y/N)")
            char = getch()
            print("'" + char + "'" + str(ord(char)))
            # 127 = backspace
            # 9 = tab
            # The chain doesn't exist already, create it
            if char == "Y":
                c = os.popen('iptables -X ' + args[2].upper())
                print("Zone " + args[2].upper() + " and all it's rules removed")
        else:
            print("That zone doesn't exist")