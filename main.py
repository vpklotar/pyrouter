#!/usr/bin/env python

import os
import sys
import shlex
import select
from commands import *
from backend import *
from inspect import isfunction

SHELL_STATUS_RUN = 1
SHELL_STATUS_STOP = 0
cmd_line = ""
current_input = ""
history = list()
history_index = 0
raw_cmd_input = ""

def isFunction2(f) :
    return 'function' in str(type(f));

def shell_loop():
    while 1 == 1:
        # Read command input
        cmd = get_line()
        if cmd is None or cmd.strip() == "":
            continue

        cmd_func = run_command(tokenize(cmd))
        if cmd_func is not None:
            cmd_func.command_function(raw_cmd_input)

def get_line():
    global current_input
    global history_index
    global history
    global raw_cmd_input
    current_input = ""
    new_line = True
    index = 0

    while True:
        if new_line == True:
            sys.stdout.write(get_breadcrum() + '> ')
            sys.stdout.flush()
            if bool(current_input.strip()):
                sys.stdout.write(current_input)
            new_line = False

        char = getch() # Char Input
        kid = ord(char) # KeyID
        # 68 = left
        # 67 = right
        # 66 = down
        # 65 = up
        # 127 = backspace
        # 63 = ? (question-mark symbol)

        if kid == 3:
            print('')
            sys.exit()
        elif kid == 9: # Tab
            current_input = find_command(tokenize(current_input))
            index = len(current_input.strip())
            print('')
            #for x in range(10):
            #print '\r',current_input
            #sys.stdout.write(find_command(cmd_line))
            #sys.stdout.write('\r')
            #sys.stdout.write(current_input)
            new_line = True
        elif kid == 13: # Enter
            print('')
            #history_index = 0
            #history.append(current_input)
            raw_cmd_input = tokenize(current_input)
            return find_command(tokenize(current_input))
        elif kid == 65:
            #cmd = history[len(history) - 1 - history_index]
            #current_input = history[len(history) - history_index]
            #sys.stdout.write('r' + cmd)
            #history_index = history_index + 1
            continue
            #print('up')
        elif kid == 66:
            continue
            #print('down')
        elif kid == 68:
            continue
            #print('left')
        elif kid == 67:
            continue
            #print('right')
        elif kid == 63: # Question-mark
            if index > 0:
                print_subcommands(tokenize(current_input))
                new_line = True
            else:
                print('')
                run_command(['help']).command_function('')
                new_line = True
        else:
            if index == 0 and kid == 127:
                continue
            elif kid == 127:
                index = index - 1
                current_input = current_input[:-1] # remove last character in the input
                sys.stdout.write('\b') # Go back on character
                sys.stdout.write(' ') # Print empty space
                sys.stdout.write('\b') # Go back one more character
                continue

            current_input = current_input + char
            index = len(current_input)
            sys.stdout.write(char)

#def print_current_line(current_line, new_line):


def print_subcommands(args):
    cmd = find_command(args)
    cmd_inst = run_command(tokenize(cmd))
    print('')
    if isinstance(cmd_inst, CommandGroup):
        for cmd in cmd_inst.get_commands():
            print(cmd)
    else:
        print('<cr>')

def find_command(args):
    level = 0
    amount = len(args)
    copy = built_in_cmds
    command_line = ""
    last_command = ""

    while level <= amount - 1:
        command = find_unique(copy, args[level]);
        if command == "":
            command = find_unique(get_current_command_set(), args[level]);
            #if command != "":
            #    copy = get_current_command_set()

        # Command is still empty, send rest of cmd as arguments
        if command == "" and last_command != "":
            #copy[last_command].command_function(args)
            return command_line
            return
        
        if command in copy:
            command_line += " " + command
            command_line = command_line.strip()
            if level >= amount - 1:
                if not isFunction2(copy[command]):
                    #copy[command]["__def"](args)
                    #print(copy[command].command_function)
                    #copy[command].command_function(args)
                    return command_line
                    #return copy[command].command_function
                else:
                    #copy[command].command_function(args)
                    return command_line
                    #return copy[command].command_function
                break
            if isinstance(copy[command], CommandGroup):
                copy = copy[command].get_commands()
            level += 1
            last_command = command
        else:
            if command != "":
                #copy[command].command_function(args)
                #return copy[command].command_function
                return command_line
            else:
                print("Command '" + args[level] + "' not found")
                break

def run_command(args):
    global built_in_cmds
    copy = built_in_cmds
    level = 0
    depth = len(args)

    for arg in args:
        if level < depth:
            cmd = copy[arg]
            #print(arg)
            if isinstance(copy[arg],  CommandGroup):
                copy = copy[arg].get_commands()
            #elif arg == copy[arg]
            level = level + 1

        if level == depth:
            return cmd

    return "BLANK"

# Function used to find a unique command in a set list
def find_unique(cmds, name):
    match = []
    for cmd in cmds:
        if cmd.startswith(name):
            match.append(cmd)
            if len(match) > 1:
                return ""
    if len(match) is 1:
        return match[0]
    else:
        return ""

def tokenize(string):
    return shlex.split(string)

def main():
    set_breadcrum(get_hostname())
    register_commands()
    shell_loop()

def register_commands():
    global built_in_cmds

    interfaces = Interface_CommandGroup()
    register_command(interfaces)

    #built_in_cmds = {
    #    "show": {
    #        "interface": {
    #            "__def": show_interface,
    #            "__desc": "Used to show interface information"
    #        },
    #        "hostname": {
    #            "__def": show_hostname,
    #            "__desc": "Show the hostname of the device"
    #        },
    #        "__def": show.__def,
    #        "__desc": "show X command can be used to diffrent types of information about this unit"
    #    },
    #    "exit": {
    #        "__def": exit_router,
    #        "__desc": "Exit this command shell"
    #    }
    #}

    # Register interface commands
    #commands = {}
    #interfaces = get_interfaces()
    #for port in interfaces:
    #    commands[port] = interface.enter

    #built_in_cmds["interface"] = commands
    #built_in_cmds["interface"]["__desc"] = "Enter interface configuration mode"*/

    # Register help command
    #help_cmd = {
    #    "__def": show_help,
    #    "__desc": "Show this help"
    #}
    h = Help()
    register_command(h)

    #exit = Exit()
    #register_command(exit)

    c = Clear()
    register_command(c)

    s = Show_CommandGroup()
    register_command(s)

    fw = Firewall_cmd()
    register_command(fw)

    ping_cmd = ping_command()
    register_command(ping_cmd)

if __name__ == "__main__":
    main()